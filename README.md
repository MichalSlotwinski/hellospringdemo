# HelloSpringDemo

#### Build image
````shell
docker build -t tomcat:9.0.58-jdk17-openjdk -f Dockerfile .
````

#### Start tomcat docker
```shell
docker run -d -p 8080:8080 --name tomcat9 tomcat:9.0.58-jdk17-openjdk
```

#### Deploy to tomcat
```shell
mvn tomcat7:deploy
```