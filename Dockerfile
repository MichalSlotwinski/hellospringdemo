FROM tomcat:9.0.58-jdk17-openjdk
RUN rm -drf webapps
RUN mv webapps.dist webapps
RUN printf '<?xml version="1.0" encoding="UTF-8"?>\n\
<tomcat-users xmlns="http://tomcat.apache.org/xml"\n\
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n\
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"\n\
              version="1.0">\n\
  <role rolename="manager-gui"/>\n\
  <role rolename="manager-script"/>\n\
  <role rolename="manager-status"/>\n\
  <role rolename="manager-jmx"/>\n\
  <role rolename="admin-gui"/>\n\
  <role rolename="admin-script"/>\n\
  <role rolename="admin-status"/>\n\
  <role rolename="admin-jmx"/>\n\
  <user username="admin" password="admin" roles="manager-gui,manager-script,manager-status,manager-jmx,admin-gui,admin-script,admin-status,admin-jmx"/>\n\
</tomcat-users>' > /usr/local/tomcat/conf/tomcat-users.xml
RUN sed -i -e 's/allow=.*/allow="^.*$"\/>/g' /usr/local/tomcat/webapps/manager/META-INF/context.xml
RUN sed -i -e 's/allow=.*/allow="^.*$"\/>/g' /usr/local/tomcat/webapps/host-manager/META-INF/context.xml
RUN sed -i -E 's/<Engine name="Catalina" defaultHost="localhost">/<Engine name="Catalina" defaultHost="localhost">\n      <Realm className="org.apache.catalina.realm.MemoryRealm"\/>/g' /usr/local/tomcat/conf/server.xml
WORKDIR "/usr/local/tomcat"
CMD ["catalina.sh", "run"]