package pl.rabbit.springdemo.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class HomeControllerTest
{
    public HomeController controller;

    @BeforeEach
    public void setup()
    {
        this.controller = new HomeController();
    }

    @Test
    @DisplayName("HomeController home test")
    public void homeTest()
    {
        Assertions.assertEquals("home", this.controller.home());
    }
}
